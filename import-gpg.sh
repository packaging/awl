#!/usr/bin/env bash

set -e

[ -z "$SIGNING_KEY" ] && echo "no key" && exit 1

echo -n "$SIGNING_KEY" | gpg --import
