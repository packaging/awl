#!/usr/bin/env bash
set -e

if [ -z "$TAG" ]; then
	# drone
	if [ -n "$DRONE_TAG" ]; then
		TAG=$DRONE_TAG
	fi
	# woodpecker
	if [ -n "$CI_COMMIT_TAG" ]; then
		TAG=$CI_COMMIT_TAG
	fi
	if [ -z "$TAG" ]; then
		echo "\$TAG not set, exiting"
		exit 1
	fi
fi

arches=("amd64" "386" "armv6" "arm64")

notag=${TAG//v/}

for arch in "${arches[@]}"; do
	wget "https://git.froth.zone/sam/awl/releases/download/$TAG/awl_${notag}_linux_${arch}.deb"
done

reprepro includedeb awl ./*.deb

mkdir -p repo
mv dists/ pool/ repo/
